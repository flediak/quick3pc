#include <math.h>
#include <string>

using namespace std;

const double tiny_c = 1e-10;

const short verbose_c = 3;//set verbose_c = 0 for silence

//===========================================================
// simulation
//===========================================================
const double L_box_c = 100; //simulation box size in Mpc
const double dim_cell_input_c = 256 ; //number of cells per axis in the input file (not relevant for readin data in ASCII_XYZ format)
const bool periodic_box_c = false;//set to "true" for taking periodic boundery condition into account when computing correlations


//===========================================================
// correlation functions
//===========================================================
const int dim_cell_c = dim_cell_input_c/4.; //number of cells per axis of grid on which correlations are computed
const int dim_JKcell_c = 4; //number of jack knife cells per axis
const int num_ang_c = 18;//number of angular bins for 3pc


//===========================================================
// input
//===========================================================
const bool input_type_A_dc_c = false; //set to "true" if input A is density contrast
const bool input_type_B_dc_c = false; //same for file B..

const string input_format_A_c = "ASCII_PIX";// set to ASCII_XYZ or ASCII_PIX for using random fields
const string input_format_B_c = "ASCII_PIX";

//OPTIONS:

//a) BINARY_FLOAT
// code loops over xyz grid and reads binary float variables

//b) BINARY_DOUBLE:
// like a) for double variables

//c) ASCII_PIX:
//ascii file with value for each pixel in one line
//format:
//val1 [delim_in_c] val2 [delim_in_c] val3 ... [delim_in_c] valN

//d) ASCII_XYZ:
//ascii file with xyz position for each data point
//format:
//x1 [delim_in_c] y1 [delim_in_c] z1
//x2 [delim_in_c] y2 [delim_in_c] z2
//...

const char delim_in_c = '\t';//delimiter for ascii input
//--------------------------------------------------------


//===========================================================
// output
//===========================================================
const string dir_out_c = "./results_test/";

const char delim_out_c = '\t';//delimiter for ascii output, = '  ';  = ',';

