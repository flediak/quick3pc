#include <iostream>
#include <stdlib.h> 
#include <math.h>
#include <vector>

using namespace std;



//===========================================================
// check if vector a and b are the same
//===========================================================
int count_same_cell( const vector < double > vec_a, const vector < double > vec_b){
   
    long num = 0;
    
    for(int i=0; i < vec_a.size(); i++){
        if(vec_a[i] == vec_b[i]){
            num++;
        }
    }
    return num;
};



//===========================================================
// initialize 2D double vector
//===========================================================
void init_vec2D(
    vector < vector < double > > &vec2D,
    const int DIM1,
    const int DIM2,
    const double val){
    
    vec2D.clear();
 
    vector < double > vec1D (DIM2, val);
    
    for(int i=0; i<DIM1; i++){vec2D.push_back(vec1D);}
};




//===========================================================
// make random field
//===========================================================
void make_random_field(const int DIM, vector < double > &field){

    int digits = 4;
    int prcn = pow(10,digits);
       
    //loop over cells
    for (int i=0; i<DIM; i++){
        for (int j=0; j<DIM; j++){
            for (int k=0; k<DIM; k++){
                
                //generate random number between 0 and 1
                double random = (rand() % prcn)/double(prcn);
                field.push_back(random);                
            }
        }
    }
}


//===========================================================
// convert cellID to grid coordinates
//===========================================================
//WARNING: max cells per axis = (4294967295)^(1÷3) = 1625
void ID_to_pos(const unsigned long ID, const int DIM, int pos[3]){
    
    pos[2] = int(ID / DIM / DIM);
    pos[1] = int(ID / DIM) - pos[2]*DIM;
    pos[0] = ID - pos[1]*DIM - pos[2]*DIM*DIM;
}


//===========================================================
// convert cellID to grid coordinates
//===========================================================
//WARNING: max cells per axis = (4294967295)^(1÷3) = 1625
unsigned long pos_to_ID(const int DIM, const int I, const int J, const int K){    
    return I + J*DIM + K*DIM*DIM;
}


//===========================================================
// transform coordinates using_periodic boundaries
//===========================================================
void make_periodic(const int DIM, int &i){
    if(i >= DIM){i = i - DIM;}
    if(i < 0){i = i + DIM;}
}


//===========================================================
// transform coordinates using_periodic boundaries
//===========================================================
int in_box(const int min, const int max, const int i, const int j, const int k){
    if(min <= i && min <= j && min <= k && i < max && j < max && k < max){
        return true;
    }else{
        return false;
    }
}


//===========================================================
// computing mean of input vector
//===========================================================
double get_mean( const vector <double> &field){

    double mean = 0;

    for (unsigned long i=0; i<field.size(); i++){
        mean = mean + field[i];
    }
    mean = mean / double(field.size());
    return mean;
}


//===========================================================
// computing mean of input vector
//===========================================================
double get_var( const vector <double> &field){

    double mean = get_mean(field);
    double var = 0;

    for (unsigned long i=0; i<field.size(); i++){
        var = var + pow(field[i]-mean,2);
    }
    var = var / double(field.size());
    return var;
}


//===========================================================
// converting density field to field of density contrasts
//===========================================================
void dtodc(vector <double> &field){
    
    double mean = get_mean(field);
    
    //convert density to density contrast
    for (unsigned long i=0; i<field.size(); i++){
        field[i] = field[i] / mean - 1;
    }
}


//===========================================================
// generate displacement vectors
//===========================================================
void make_shells(
    double r1, double dr1, double r2, double dr2,
    vector < vector < int > > &dvec1,
    vector < vector < int > > &dvec2
){
    
    double rupsq1 = pow(r1 + dr1, 2);
    double rdownsq1 = pow(r1 - dr1, 2);

    double rupsq2 = pow(r2 + dr2, 2);
    double rdownsq2 = pow(r2 - dr2, 2);
    
    
    //shell1
    for(int i = -(r1); i <= (r1); i++){
        for(int j = -(r1); j <= (r1); j++){
            for(int k = 0; k <= (r1); k++){

                double rsq = i*i + j*j + k*k;
            
                if(rdownsq1 < rsq && rsq < rupsq1){
                    
                    vector < int > dvec_sub;
                    dvec_sub.push_back(i);
                    dvec_sub.push_back(j);
                    dvec_sub.push_back(k);
                    dvec1.push_back(dvec_sub);
                }
            }
        }
    }
    
    
    //shell2   
    for(int i = - (r2); i <= (r2); i++){
        for(int j = - (r2); j <= (r2); j++){
            for(int k = - (r2); k <= (r2); k++){

                double rsq = i*i + j*j + k*k;

                if(rdownsq2 < rsq && rsq < rupsq2){
                                
                    vector < int > dvec_sub;
                    dvec_sub.push_back(i);
                    dvec_sub.push_back(j);
                    dvec_sub.push_back(k);
                    dvec2.push_back(dvec_sub);
                }
            }
        }
    }
    
}


//===========================================================
// get angles between two sets of displacement vectors
//===========================================================
void get_angles(
    vector < vector < int > > dvec1,
    vector < vector < int > > dvec2,
    vector < vector < double > > &alpha
){
    for(int i = 0; i < dvec1.size(); i++){
        
        vector < double > alphasub;
        
        //absolute value of triangle leg r1
        double absr1 = sqrt(double(
            dvec1[i][0]*dvec1[i][0] + 
            dvec1[i][1]*dvec1[i][1] +
            dvec1[i][2]*dvec1[i][2]        
        ));

        
        
        for(int j = 0; j < dvec2.size(); j++){
            
            //absolute value of triangle leg r1
            double absr2 = sqrt(double(
                dvec2[j][0]*dvec2[j][0] + 
                dvec2[j][1]*dvec2[j][1] +
                dvec2[j][2]*dvec2[j][2]
            ));
            
            
            // skalar product <r1 r2>
            double skapro_r1r2 = double(
                dvec1[i][0]*dvec2[j][0] + 
                dvec1[i][1]*dvec2[j][1] +
                dvec1[i][2]*dvec2[j][2]

            );
    
            //normalize
            skapro_r1r2 = skapro_r1r2 / (absr1*absr2);
            
            //fixing numerical problem when triangles are completely collapsed or relaxed
            if((skapro_r1r2) >=  0.999999){ skapro_r1r2 =  1.; }
            if((skapro_r1r2) <= -0.999999){ skapro_r1r2 = -1.; }
            
            //angle between r1 and r2
            double angle = 180 - acos( skapro_r1r2 ) * 180.0 / M_PI;

            alphasub.push_back(angle);
            
        }
        alpha.push_back(alphasub);
    }
}




//===========================================================
// get angular bin between two sets of displacement vectors
//===========================================================
void get_ang_bin(
    const int num_ang,
    const vector < vector < double > > alpha,
    vector < vector < short > > &alpha_bin
){
    double dalpha = 180./double(num_ang);
    
    for(int i = 0; i < alpha.size(); i++){
        
        vector < short > alpha_bin_sub;
        
        for(int j = 0; j < alpha[i].size(); j++){
            int bin = alpha[i][j]/dalpha;
            
            //make sure, that triangles with opening angle=180 degree end up in largest angular bin
            if(bin == num_ang){bin = num_ang-1;}
            
            //just a double check, actually not needed
            if(bin>=0 && bin<num_ang){
                alpha_bin_sub.push_back(bin);
            }
        }
        
        alpha_bin.push_back(alpha_bin_sub);
    }
}

//===========================================================
// get mean angel in each bin
//===========================================================
void get_ang_mean(
    const int num_ang,
    const vector < vector < double > > alpha,
    const vector < vector < short > > alpha_bin,
    vector < double > &alpha_mean
){
    
    alpha_mean.clear();
    alpha_mean.assign(num_ang,0);
    vector <double> norm(num_ang,0);
    
    for(int i = 0; i < alpha.size(); i++){
        for(int j = 0; j < alpha[i].size(); j++){

            short bin = alpha_bin[i][j];
            
            if(0 <= bin && bin < num_ang){
                alpha_mean[bin] = alpha_mean[bin] + alpha[i][j];
                norm[bin] = norm[bin] + 1;
            }else{
                cout<<"#### ERROR [get_ang_bin]: check angular binning"<<endl;
                exit(0);               
            }
        }
    }
    
    for(int i = 0; i < alpha_mean.size(); i++){
        alpha_mean[i] = alpha_mean[i] / norm[i];
    }    
}


//===========================================================
// get mean size of third triangle leg for in each bin
//===========================================================
void get_r3_mean(
    const double r1,
    const double r2,
    vector < double > &alpha_vec,
    vector < double > &r3_vec
){
    
    r3_vec.clear();
    
    for(int i = 0; i < alpha_vec.size(); i++){
        
         double r3 = sqrt(r1*r1 + r2*r2 - 2*r1*r2*cos(alpha_vec[i] * M_PI / 180.0));
        
         r3_vec.push_back(r3);
    }
    
}


//===========================================================
// normalize 1D vector
//===========================================================
void norm_1Dvec(vector < double > &vec, const vector < double > norm){
    
    if(vec.size() != norm.size()){
        cout<<"#### ERROR [norm_1Dvec]: input vectors in norm_1Dvec do not have the same dimension"<<endl;
        exit(0);
    }

        
    for(int i=0; i < vec.size(); i++){
        if(norm[i] != 0){
            vec[i] = vec[i] / norm[i];
        }else{
            vec[i] = 0;
        }
    }
}


//===========================================================
// normalize 2D vector
//===========================================================
void norm_2Dvec(vector < vector < double > > &vec, const vector < vector < double > > norm){
    
    if(vec.size() != norm.size()){
        cout<<"#### ERROR [norm_2Dvec]: input vectors in norm_2Dvec do not have the same dimension"<<endl;
        exit(0);
    }

        
    for(int i=0; i < vec.size(); i++){
        for(int j=0; j < vec[i].size(); j++){
            if(norm[i][j] != 0){
                vec[i][j] = vec[i][j] / norm[i][j];
            }else{
                vec[i][j] = 0;
            }
        }
    }
}




//===========================================================
// make symmetric 3pc cross-correlation
//===========================================================
void make_Z_sym(
    const vector < double > Z_a1b2b3,
    const vector < double > Z_b1a2b3, 
    const vector < double > Z_b1b2a3,
    vector < double > &Z_sym)
{
    Z_sym.clear();
    
    //loop over angular bins
    for(int i=0; i<Z_a1b2b3.size(); i++){

        double Z_sym_bin = (Z_a1b2b3[i] + Z_b1a2b3[i] + Z_b1b2a3[i]) / 3.; 
                
        Z_sym.push_back(Z_sym_bin);
    }
}



//===========================================================
// make hierarchical 3pc
//===========================================================
void make_ZH(
    const double Xi_12,
    const double Xi_23,
    const vector < double > Xi_13,
    vector < double > &ZH
){
    //loop over angular bins
    for(int i=0; i<Xi_13.size(); i++){
          
        double ZH_bin = 
        Xi_12*Xi_13[i] +
        Xi_12*Xi_23 +
        Xi_23*Xi_13[i];
        
        ZH.push_back(ZH_bin);
    }
}


//===========================================================
// make reduced 3pc
//===========================================================
void make_Q(
    const vector < double > Z,
    const vector < double > ZH,
    vector < double > &Q
){

    //loop over angular bins
    for(int i=0; i<Z.size(); i++){
        
        double Q_bin = 0;
        
        if(ZH[i] != 0){
            Q_bin =  Z[i] / ZH[i];
        }
        Q.push_back(Q_bin);
    }
}


//===========================================================
// make normalized covariance
//===========================================================
void get_normalized_cov(
    const vector < vector < double > > cov,
    vector < vector < double > > &cov_norm
){

    cov_norm.clear();
    
    init_vec2D(cov_norm, cov.size(), cov.size(), 0);
    
    for(int i=0; i< cov.size(); i++){
        for(int j=0; j< cov.size(); j++){
            cov_norm[i][j] = cov[i][j] / sqrt(cov[i][i])  / sqrt(cov[j][j]); 
        }
    }
}


//===========================================================
// print cell number when conputing 3pc
//===========================================================
void print_cell_num(const int DIM, const int i){
    if (i > 0){
        cout<< DIM - i <<"; ";
        cout.flush();
        if (i % 20 == 0){ cout<<endl; }
    }
}
