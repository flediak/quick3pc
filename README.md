## quick3PC
quick3PC is an implementation of the algorithm for computing **3PCF**
described by http://adsabs.harvard.edu/abs/2002MNRAS.333..443B.

The code searches triangle with two legs of fixed sizes r1 and r2
(with tollerance +/-dr1 and  +/-dr2) on a grid and measures the 3PCF
for different triangle opening angles

The code also delivers measurements of the 2PCF along the third triangle leg with variable size r3

The code provides Jack Knife error and covariances estimates for all statsitics


currently suported input formats:
- ASCII file with xyz data (ASCII_XYZ)
- ASCII file with pixel data on xyz grid (ASCII_PIX)
- binary files with pixel data on xyz grid

Details on the ASCII formats are described in parameters.h

A simple exmaple for running the code is given in run_quick3PC.sh
This script first generates random input data set with ./make_rand_cat
and then computs the auto- and cross-correlations with ./quick3PC

The script can be run from the with source run_quick3PC.h

---

## compiling and running quick3PC

i) edit parameters.h file to specify
- simulation details
- format of input and ouput data
- grid for 3PCF computation

ii) compile the code with make (also after changes in parameters.h)

iii) run code

a) ./quick3pc <r1> <dr1> <r2> <dr2> <suffix_output> <filename_in A>
for computing auto-correlation:

b) ./quick3pc <r1> <dr1> <r2> <dr2> <suffix_output> <filename_in A> <filename_in B>
for computing auto- and cross-correlation:

suffix_output identifies the various output files

---

## OUTPUT

**3PCF_suffix_output.dat** contains 2PCF and 3PCF measurements for different triangle opening angles

1 bin
2 central angle in bin,
3 mean angle in bin,
4,5 2PCF, err,
6,7 3PCF, err,
8,9 hierarchical 3PCF, err
10,11 reduced 3PCF, err


**cov_suffix_output.dat** contains normalized covariances for 2PCF and 3PCF measurements

1,2 bin1, bin2
3,4 central angle 1, central angle 2
5,6 mean angle 1, mean angle 2
7 cov 2PCF
8 cov 3PCF
9 cov hierarchical 3PCF
10 cov reduced 3PCF

see io_data.h for details

--

## make_rand_cat

Generats ASCII file with random field which can be used as input file for quick3PC

The type of ASCII format is set by the options ASCII_XYZ and ASCII_PIX in make_rand_cat.cpp

The characteristics of the box side length and grid format are specified in parameters.h

compile make
run ./make_rand_cat <filename_out>

