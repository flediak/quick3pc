//Kai Hoffmann
//Tsinghua Center for Astrophysics 2018

#include <iostream>
#include <math.h>
#include <string>
//#include <experimental/filesystem>
#include "./quick3PC.h"
#include "./parameters.h"
#include "./io_data.h"
#include "./jack_knife.h"
#include "./triplets.h"

using namespace std;

//===================================================================================================
//===================================================================================================
//=========================================   MAIN CODE   ===========================================
//===================================================================================================
//===================================================================================================
int main(int argc, char **argv){

    
    if(verbose_c > 0){
        cout << endl;
        cout << "#===========================================================" << endl;
        cout << "#======================== quick3PC =========================" << endl;
        cout << "#===========================================================" << endl;
        cout << endl;
    }

    
    
    //=========================================================================================
    if(verbose_c > 2){ cout << endl <<"# read input arguments" << endl;}
    //=========================================================================================
    
    bool CROSS_CORR = false;
    
    //check number of arguments
    if(argc == 8){

        if(verbose_c > 1){cout<<"# starting to computing auto-correlation of field A" << endl << endl;}

    }else if(argc == 10){

        if(verbose_c > 1){cout<<"# starting to computing auto- and cross-correlations of field A and B" << endl << endl;}

        CROSS_CORR = true;
    
    }else{
        cout<<"# USAGE FOR AUTO-CORRELATION: ./quick3pc <r1> <dr1> <r2> <dr2> <suffix_output> <tag file A> <filename_in A>" << endl;
        cout<<"# USAGE FOR CROSS-CORRELATION: ./quick3pc <r1> <dr1> <r2> <dr2> <suffix_output> <tag file A> <tag file B> <filename_in A> <filename_in B>" << endl << endl;
        return -1;
    }
    
    //read arguments                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
    double r1 = atof(argv[1]);
    double dr1 = atof(argv[2]);
    double r2 = atof(argv[3]);
    double dr2 = atof(argv[4]);

    string suffix_output = argv[5];
    
    string tag_A, tag_B, filename_in_A, filename_in_B;
    
    if(!CROSS_CORR){
        tag_A = argv[6];
        filename_in_A = argv[7];
    }else{
        tag_A = argv[6];
        tag_B = argv[7];
        filename_in_A = argv[8];
        filename_in_B = argv[9];
    }
    
    //side length of grid cells
    const double L_cell_c = L_box_c / double(dim_cell_c);

    if(verbose_c > 1){
        cout<< "# ============ INPUT PARAMETERS ============" << endl;
        cout<< "# L_box_c = "<< L_box_c << endl;
        cout<< "# L_cell_c = "<< L_cell_c << endl;
        cout<< "# number of cells per axis: dim_cell_c = "<< dim_cell_c << endl;
        cout<< "# number of Jack Knife samples per axis: dim_JKcell_c = " << dim_JKcell_c << endl; 
        cout<< "# number of angular bins: num_ang_c = " << num_ang_c << endl;
        cout<< "# r1 = " << r1 <<" +/- "<<dr1 <<" cells <=> "<< r1*L_cell_c <<" +/- "<<dr1*L_cell_c<<" Mpc"<<endl;  
        cout<< "# r2 = " << r2 <<" +/- "<<dr2 <<" cells <=> "<< r2*L_cell_c <<" +/- "<<dr2*L_cell_c<<" Mpc"<<endl;
        cout<< "# input_format_A_c: "<< input_format_A_c << endl;
        if(CROSS_CORR){ cout<< "# input_format_B_c: "<< input_format_B_c << endl;}
        cout<< "# ==========================================" << endl << endl;
    }
    
    //check if grid is too big
    if(dim_cell_c > 1625){
        cout<<"#### ERROR: dim_cell_c too high. Choose smaller value or use larger data type for ID_CELL ####" <<endl;
        return -1;
    }
  
        
    //check if output directory exsists. if not, create it
    if(!directory_exists(verbose_c, dir_out_c)){
        make_directory(verbose_c, dir_out_c);
    }
    
    
    //=========================================================================================
    if(verbose_c > 2){ cout << endl <<"# read input files" << endl;}
    //=========================================================================================

    //file A
    if(verbose_c > 0){ cout << endl <<"# reading field A: "<< filename_in_A <<endl;}
    
    vector < double > dc_a;
    if(input_format_A_c== "BINARY_FLOAT"){ 
        read_binary_float(dim_cell_input_c, dim_cell_c, filename_in_A, dc_a);
    }else if(input_format_A_c== "BINARY_DOUBLE"){
        read_binary_double(dim_cell_input_c, dim_cell_c, filename_in_A, dc_a);
    }else if(input_format_A_c== "ASCII_PIX"){
        read_ascii_pix(delim_in_c, filename_in_A,  dc_a);  
    }else if(input_format_A_c== "ASCII_XYZ"){
        read_ascii_xyz(verbose_c, L_box_c, dim_cell_c, delim_in_c, filename_in_A,  dc_a);
    }else{
        cout<<"# ERROR: input format for file A :" << input_format_A_c << " unknown"<< endl;
        return -1;
    }
    
    //file B
    vector < double > dc_b;
    if(CROSS_CORR){
        
        if(verbose_c > 0){ cout << endl <<"# reading field B: "<< filename_in_B <<endl;}
        
        if(input_format_B_c== "BINARY_FLOAT"){ 
            read_binary_float(dim_cell_input_c, dim_cell_c, filename_in_B, dc_b);
        }else if(input_format_B_c== "BINARY_DOUBLE"){
            read_binary_double(dim_cell_input_c, dim_cell_c, filename_in_B, dc_b);
        }else if(input_format_B_c== "ASCII_PIX"){
            read_ascii_pix(delim_in_c, filename_in_B, dc_b);
        }else if(input_format_B_c== "ASCII_XYZ"){
            read_ascii_xyz(verbose_c, L_box_c, dim_cell_c, delim_in_c, filename_in_B,  dc_b);
        }else{
            cout<<"# ERROR: input format for file B :" << input_format_B_c << " unknown"<< endl;
            return -1;
        }
    }
    
    //convert input data to density contrast
    if(!input_type_A_dc_c){
        if(verbose_c > 0){ cout << endl <<"# convert field A to density contrast"<<endl;}
        dtodc(dc_a);
    }

    if(CROSS_CORR){
        if(!input_type_B_dc_c){
            if(verbose_c > 0){ cout << endl <<"# convert field B to density contrast"<<endl;}
            dtodc(dc_b); 
        }
    }

    //check dinemsion of input grid A
    if(dc_a.size() != pow(dim_cell_c,3)){
        cout<<"#### ERROR: number of cells in input file A is not dim_cell_c^3 ####" <<endl;
        return -1;
    }

    if(CROSS_CORR){
        //check dinemsion of input grid B
        if(dc_b.size() != pow(dim_cell_c,3)){
            cout<<"#### ERROR: number of cells in input file B is not dim_cell_c^3 ####" <<endl;
            return -1;
        }

        //check if files A and B are the same
        if(count_same_cell(dc_a, dc_b) == pow(dim_cell_c,3)){
            cout<<endl<<"#### WARNING: input file A and B are identical ####" <<endl;
        }
    }
       
    //check if mean density contrast is (close to) zero
    if(get_mean(dc_a) >  tiny_c){
        cout<<endl<<"#### WARNING: mean density contrast for input file A: <dc> = " << get_mean(dc_a) << " (should be zero) ####" <<endl;
    }

    if(CROSS_CORR){
        if(get_mean(dc_b) >  tiny_c){
            cout<<endl<<"#### WARNING: mean density contrast for input file B: <dc> = " << get_mean(dc_b) << " (should be zero) ####" <<endl;
        }
    }
    
    
    //=========================================================================================
    if(verbose_c > 1){ cout << endl <<"# generate shells of displacement vectors" << endl;}
    //=========================================================================================

    //make shells
    vector < vector < int > > dvec1,  dvec2;
    make_shells(r1, dr1, r2, dr2, dvec1, dvec2);
       
    //get triangle opening angles between pairs of displacement vectors
    vector < vector < double > > alpha;
    get_angles(dvec1, dvec2, alpha);
    
    //get angular bin for each pair of displacement vectors
    vector < vector < short > > alpha_bin;
    get_ang_bin(num_ang_c, alpha, alpha_bin);

    //get mean opening angle for each angular bin
    vector < double > alpha_mean;
    get_ang_mean(num_ang_c, alpha, alpha_bin, alpha_mean);

    //get mean size of third triangle leg for each opening angle
    vector < double > r3_mean;
    get_r3_mean(r1*L_cell_c, r2*L_cell_c, alpha_mean, r3_mean);

    if(verbose_c > 1){
        cout<<"# - number of displacement vectors on shell 1: "<< dvec1.size() <<", on shell 2: "<< dvec2.size() << endl;
    }

    
    //=========================================================================================
    if(verbose_c > 2){ cout << endl <<"# initializing variables for 3pc computation and JK sampling" << endl;}
    //=========================================================================================
 
    const int num_JKcell_c = pow(dim_JKcell_c,3); //total number of jack knife cells
    
    //1D vectors for storing correlations from JK cells and JK samples
    vector < double >
    Xi_a1a2_JK(num_JKcell_c, 0),
    Xi_b1b2_JK(num_JKcell_c, 0),
    Xi_a1b2_JK(num_JKcell_c, 0),
    norm_12_JK(num_JKcell_c, 0),
    
    Xi_a2a3_JK(num_JKcell_c, 0),
    Xi_b2b3_JK(num_JKcell_c, 0),
    Xi_a2b3_JK(num_JKcell_c, 0),
    norm_23_JK(num_JKcell_c, 0),

    Xi_a1a2_JKsamp(num_JKcell_c, 0),
    Xi_b1b2_JKsamp(num_JKcell_c, 0),
    Xi_a1b2_JKsamp(num_JKcell_c, 0),
    norm_12_JKsamp(num_JKcell_c, 0),
    
    Xi_a2a3_JKsamp(num_JKcell_c, 0),
    Xi_b2b3_JKsamp(num_JKcell_c, 0),
    Xi_a2b3_JKsamp(num_JKcell_c, 0),
    norm_23_JKsamp(num_JKcell_c, 0);

    
    //2D vectors for storing correlations from JK cells and JK samples
    vector < vector < double > >
    Xi_a1a3_JK, Xi_b1b3_JK, Xi_a1b3_JK, Z_a1a2a3_JK, Z_b1b2b3_JK, Z_a1b2b3_JK, Z_b1a2b3_JK, Z_b1b2a3_JK, norm_13_JK, norm_123_JK,
    Xi_a1a3_JKsamp, Xi_b1b3_JKsamp, Xi_a1b3_JKsamp, Z_a1a2a3_JKsamp, Z_b1b2b3_JKsamp, Z_a1b2b3_JKsamp, Z_b1a2b3_JKsamp, Z_b1b2a3_JKsamp, norm_13_JKsamp, norm_123_JKsamp;
    
    //initialize to zero
    init_vec2D(Xi_a1a3_JK, num_JKcell_c, num_ang_c, 0);
    init_vec2D(Xi_b1b3_JK, num_JKcell_c, num_ang_c, 0);
    init_vec2D(Xi_a1b3_JK, num_JKcell_c, num_ang_c, 0);
    init_vec2D(norm_13_JK, num_JKcell_c, num_ang_c, 0);
    
    init_vec2D(Z_a1a2a3_JK, num_JKcell_c, num_ang_c, 0);
    init_vec2D(Z_b1b2b3_JK, num_JKcell_c, num_ang_c, 0);
    init_vec2D(Z_a1b2b3_JK, num_JKcell_c, num_ang_c, 0);
    init_vec2D(Z_b1a2b3_JK, num_JKcell_c, num_ang_c, 0);
    init_vec2D(Z_b1b2a3_JK, num_JKcell_c, num_ang_c, 0);
    init_vec2D(norm_123_JK, num_JKcell_c, num_ang_c, 0);

    init_vec2D(Xi_a1a3_JKsamp, num_JKcell_c, num_ang_c, 0);
    init_vec2D(Xi_b1b3_JKsamp, num_JKcell_c, num_ang_c, 0);
    init_vec2D(Xi_a1b3_JKsamp, num_JKcell_c, num_ang_c, 0);
    init_vec2D(norm_13_JKsamp, num_JKcell_c, num_ang_c, 0);
    
    init_vec2D(Z_a1a2a3_JKsamp, num_JKcell_c, num_ang_c, 0);
    init_vec2D(Z_b1b2b3_JKsamp, num_JKcell_c, num_ang_c, 0);
    init_vec2D(Z_a1b2b3_JKsamp, num_JKcell_c, num_ang_c, 0);
    init_vec2D(Z_b1a2b3_JKsamp, num_JKcell_c, num_ang_c, 0);
    init_vec2D(Z_b1b2a3_JKsamp, num_JKcell_c, num_ang_c, 0);
    init_vec2D(norm_123_JKsamp, num_JKcell_c, num_ang_c, 0);
    
    
    //=========================================================================================
    if(verbose_c > 0){ cout<<endl<<endl<<"# ---- compute 3pc -----" << endl; }
    //=========================================================================================
    time_t start;
    time (&start);
    
    
    //auto field A
    if(verbose_c > 1){cout << endl << "# auto 3PC of field A "<<endl;}
    compute_3pc(verbose_c, periodic_box_c, dim_cell_c, dim_JKcell_c, dc_a, dvec1, dvec2, alpha_bin, Xi_a1a2_JK, Xi_a2a3_JK, Xi_a1a3_JK, Z_a1a2a3_JK);
    
    
    if(CROSS_CORR){

        //auto field B
        if(verbose_c > 1){cout << endl << "# auto 3PC of field B "<<endl;}
        compute_3pc(verbose_c, periodic_box_c, dim_cell_c, dim_JKcell_c, dc_b, dvec1, dvec2, alpha_bin, Xi_b1b2_JK, Xi_b2b3_JK, Xi_b1b3_JK, Z_b1b2b3_JK);
        
        //cross field A x B
        if(verbose_c > 1){cout << endl << "# cross 3PC between field A and B"<<endl;}
        compute_3pcc(verbose_c, periodic_box_c, dim_cell_c, dim_JKcell_c, dc_a, dc_b, dvec1, dvec2, alpha_bin, Xi_a1b2_JK, Xi_a2b3_JK, Xi_a1b3_JK, Z_a1b2b3_JK, Z_b1a2b3_JK, Z_b1b2a3_JK);
    }
    
    
    //get norm
    if(verbose_c > 1){cout << endl << "# 3PC norm"<<endl;}
    get_3pc_norm(verbose_c, periodic_box_c, dim_cell_c, dim_JKcell_c, dvec1, dvec2, alpha_bin, norm_12_JK, norm_23_JK, norm_13_JK, norm_123_JK);
    
    
    time_t end;
    time (&end);
    
    if(verbose_c > 0){ cout<<endl <<"# "<< double(difftime (end,start)) << " seconds for 3PC computation" << endl; }


    //=========================================================================================
    if(verbose_c > 1){ cout<<"# sum over measurements in JK cells to get results for full box" << endl; }
    //=========================================================================================
    
    //2pc for leg r12
    double Xi_a1a2, Xi_b1b2,  Xi_a1b2, norm_12;
    sumJKcells_1Dvec(Xi_a1a2_JK, Xi_a1a2);
    sumJKcells_1Dvec(Xi_b1b2_JK, Xi_b1b2);
    sumJKcells_1Dvec(Xi_a1b2_JK, Xi_a1b2);
    sumJKcells_1Dvec(norm_12_JK, norm_12);

    //2pc for leg r23
    double Xi_a2a3, Xi_b2b3, Xi_a2b3, norm_23;
    sumJKcells_1Dvec(Xi_a2a3_JK, Xi_a2a3);
    sumJKcells_1Dvec(Xi_b2b3_JK, Xi_b2b3);
    sumJKcells_1Dvec(Xi_a2b3_JK, Xi_a2b3);
    sumJKcells_1Dvec(norm_23_JK, norm_23);
    
    //2pc for leg r13
    vector < double > Xi_a1a3, Xi_b1b3, Xi_a1b3, norm_13;
    sumJKcells_2Dvec(Xi_a1a3_JK, Xi_a1a3);
    sumJKcells_2Dvec(Xi_b1b3_JK, Xi_b1b3);
    sumJKcells_2Dvec(Xi_a1b3_JK, Xi_a1b3);
    sumJKcells_2Dvec(norm_13_JK, norm_13);
    
    //3pc
    vector < double > Z_a1a2a3, Z_b1b2b3, Z_a1b2b3, Z_b1a2b3, Z_b1b2a3, norm_123;
    sumJKcells_2Dvec(Z_a1a2a3_JK, Z_a1a2a3);
    sumJKcells_2Dvec(Z_b1b2b3_JK, Z_b1b2b3);
    sumJKcells_2Dvec(Z_a1b2b3_JK, Z_a1b2b3);
    sumJKcells_2Dvec(Z_b1a2b3_JK, Z_b1a2b3);
    sumJKcells_2Dvec(Z_b1b2a3_JK, Z_b1b2a3);
    sumJKcells_2Dvec(norm_123_JK, norm_123);
    //(norm_123 and norm_13 are the same..)
        

    //normalize
    Xi_a1a2 = Xi_a1a2 / norm_12;
    Xi_b1b2 = Xi_b1b2 / norm_12;
    Xi_a1b2 = Xi_a1b2 / norm_12;
    
    Xi_a2a3 = Xi_a2a3 / norm_23;
    Xi_b2b3 = Xi_b2b3 / norm_23;
    Xi_a2b3 = Xi_a2b3 / norm_23;
    
    for(int i=0; i < Xi_a1a3.size(); i++){ Xi_a1a3[i] = Xi_a1a3[i] / norm_13[i]; }
    for(int i=0; i < Xi_b1b3.size(); i++){ Xi_b1b3[i] = Xi_b1b3[i] / norm_13[i]; }
    for(int i=0; i < Xi_a1b3.size(); i++){ Xi_a1b3[i] = Xi_a1b3[i] / norm_13[i]; }

    for(int i=0; i < Z_a1a2a3.size(); i++){ Z_a1a2a3[i] = Z_a1a2a3[i] / norm_123[i]; }
    for(int i=0; i < Z_b1b2b3.size(); i++){ Z_b1b2b3[i] = Z_b1b2b3[i] / norm_123[i]; }

    for(int i=0; i < Z_a1b2b3.size(); i++){ Z_a1b2b3[i] = Z_a1b2b3[i] / norm_123[i]; }
    for(int i=0; i < Z_b1a2b3.size(); i++){ Z_b1a2b3[i] = Z_b1a2b3[i] / norm_123[i]; }
    for(int i=0; i < Z_b1b2a3.size(); i++){ Z_b1b2a3[i] = Z_b1b2a3[i] / norm_123[i]; }

    
    //=========================================================================================
    if(verbose_c > 1){ cout<<"# make JK samples"<<endl; }
    //=========================================================================================
    
    //2pc for leg r12
    makeJKsamp_1Dvec(Xi_a1a2_JK, Xi_a1a2_JKsamp);
    makeJKsamp_1Dvec(Xi_b1b2_JK, Xi_b1b2_JKsamp);
    makeJKsamp_1Dvec(Xi_a1b2_JK, Xi_a1b2_JKsamp);
    makeJKsamp_1Dvec(norm_12_JK, norm_12_JKsamp);
    
    //2pc for leg r23
    makeJKsamp_1Dvec(Xi_a2a3_JK, Xi_a2a3_JKsamp);
    makeJKsamp_1Dvec(Xi_b2b3_JK, Xi_b2b3_JKsamp);
    makeJKsamp_1Dvec(Xi_a2b3_JK, Xi_a2b3_JKsamp);
    makeJKsamp_1Dvec(norm_23_JK, norm_23_JKsamp);
        
    //2pc for leg r13
    makeJKsamp_2Dvec(Xi_a1a3_JK, Xi_a1a3_JKsamp);
    makeJKsamp_2Dvec(Xi_b1b3_JK, Xi_b1b3_JKsamp);
    makeJKsamp_2Dvec(Xi_a1b3_JK, Xi_a1b3_JKsamp);
    makeJKsamp_2Dvec(norm_13_JK, norm_13_JKsamp);
    
    //3pc
    makeJKsamp_2Dvec(Z_a1a2a3_JK, Z_a1a2a3_JKsamp);
    makeJKsamp_2Dvec(Z_b1b2b3_JK, Z_b1b2b3_JKsamp);
    makeJKsamp_2Dvec(Z_a1b2b3_JK, Z_a1b2b3_JKsamp);
    makeJKsamp_2Dvec(Z_b1a2b3_JK, Z_b1a2b3_JKsamp);
    makeJKsamp_2Dvec(Z_b1b2a3_JK, Z_b1b2a3_JKsamp);
    makeJKsamp_2Dvec(norm_123_JK, norm_123_JKsamp);
    //(norm_123 and norm_13 are the same..)
    
    
    //normalize 2pc for leg r12
    norm_1Dvec(Xi_a1a2_JKsamp, norm_12_JKsamp);
    norm_1Dvec(Xi_b1b2_JKsamp, norm_12_JKsamp);
    norm_1Dvec(Xi_a1b2_JKsamp, norm_12_JKsamp);
    
    //normalize 2pc for leg r23
    norm_1Dvec(Xi_a2a3_JKsamp, norm_23_JKsamp);
    norm_1Dvec(Xi_b2b3_JKsamp, norm_23_JKsamp);
    norm_1Dvec(Xi_a2b3_JKsamp, norm_23_JKsamp);
        
    //normalize 2pc for leg r13
    norm_2Dvec(Xi_a1a3_JKsamp, norm_13_JKsamp);
    norm_2Dvec(Xi_b1b3_JKsamp, norm_13_JKsamp);
    norm_2Dvec(Xi_a1b3_JKsamp, norm_13_JKsamp);
    
    //normalize 3pc
    norm_2Dvec(Z_a1a2a3_JKsamp, norm_123_JKsamp);
    norm_2Dvec(Z_b1b2b3_JKsamp, norm_123_JKsamp);
    norm_2Dvec(Z_a1b2b3_JKsamp, norm_123_JKsamp);
    norm_2Dvec(Z_b1a2b3_JKsamp, norm_123_JKsamp);
    norm_2Dvec(Z_b1b2a3_JKsamp, norm_123_JKsamp);   
    

    //=========================================================================================
    if(verbose_c > 1){ cout << endl << "# define symetric, hierarchical and reduced 3pc" << endl; }
    //=========================================================================================
    
    //symetric (full box)
    vector < double > Z_sym;
    make_Z_sym(Z_a1b2b3, Z_b1a2b3, Z_b1b2a3, Z_sym);
    
    //hierarchical (full box)
    vector < double > ZH_a, ZH_b, ZH_ab;
    make_ZH(Xi_a1a2, Xi_a2a3, Xi_a1a3, ZH_a);
    make_ZH(Xi_b1b2, Xi_b2b3, Xi_b1b3, ZH_b);
    make_ZH(Xi_a1b2, Xi_a2b3, Xi_a1b3, ZH_ab);
    
    //reduced (full box)
    vector < double > Q_a1a2a3, Q_b1b2b3, Q_sym;
    make_Q(Z_a1a2a3, ZH_a, Q_a1a2a3);
    make_Q(Z_b1b2b3, ZH_b, Q_b1b2b3);
    make_Q(Z_sym, ZH_ab, Q_sym);

    
    
    //symetric (JK samples)
    vector < vector < double > > Z_sym_JKsamp;
    make_Z_sym_JK(Z_a1b2b3_JKsamp, Z_b1a2b3_JKsamp, Z_b1b2a3_JKsamp, Z_sym_JKsamp);
    
    //hierarchical (JK samples)
    vector < vector < double > > ZH_a_JKsamp, ZH_b_JKsamp, ZH_ab_JKsamp;
    make_ZH_JK(Xi_a1a2_JKsamp, Xi_a2a3_JKsamp, Xi_a1a3_JKsamp, ZH_a_JKsamp);
    make_ZH_JK(Xi_b1b2_JKsamp, Xi_b2b3_JKsamp, Xi_b1b3_JKsamp, ZH_b_JKsamp);
    make_ZH_JK(Xi_a1b2_JKsamp, Xi_a2b3_JKsamp, Xi_a1b3_JKsamp, ZH_ab_JKsamp);
    
    //reduced (JK samples)
    vector < vector < double > > Q_a1a2a3_JKsamp, Q_b1b2b3_JKsamp, Q_sym_JKsamp;
    make_Q_JK(Z_a1a2a3_JKsamp, ZH_a_JKsamp, Q_a1a2a3_JKsamp);
    make_Q_JK(Z_b1b2b3_JKsamp, ZH_b_JKsamp, Q_b1b2b3_JKsamp);
    make_Q_JK(Z_sym_JKsamp, ZH_ab_JKsamp, Q_sym_JKsamp);
    

    
    //=========================================================================================
    if(verbose_c > 1){ cout << endl << "# make Jack Knife covariance " << endl; }
    //=========================================================================================
    
    //make covariance
    vector < vector < double > >
    cov_Xi_a1a3, cov_Xi_b1b3, cov_Xi_a1b3,
    cov_Z_a1a2a3, cov_Z_b1b2b3, cov_Z_sym,
    cov_ZH_a, cov_ZH_b, cov_ZH_ab,
    cov_Q_a1a2a3, cov_Q_b1b2b3, cov_Q_sym;
   
    make_JKcov(Xi_a1a3, Xi_a1a3_JKsamp, cov_Xi_a1a3);
    make_JKcov(Xi_b1b3, Xi_b1b3_JKsamp, cov_Xi_b1b3);
    make_JKcov(Xi_a1b3, Xi_a1b3_JKsamp, cov_Xi_a1b3);

    make_JKcov(Z_a1a2a3, Z_a1a2a3_JKsamp, cov_Z_a1a2a3);
    make_JKcov(Z_b1b2b3, Z_b1b2b3_JKsamp, cov_Z_b1b2b3);
    make_JKcov(Z_sym, Z_sym_JKsamp, cov_Z_sym);

    make_JKcov(ZH_a, ZH_a_JKsamp, cov_ZH_a);
    make_JKcov(ZH_b, ZH_b_JKsamp, cov_ZH_b);
    make_JKcov(ZH_ab, ZH_ab_JKsamp, cov_ZH_ab);
    
    make_JKcov(Q_a1a2a3, Q_a1a2a3_JKsamp, cov_Q_a1a2a3);
    make_JKcov(Q_b1b2b3, Q_b1b2b3_JKsamp, cov_Q_b1b2b3);
    make_JKcov(Q_sym, Q_sym_JKsamp, cov_Q_sym);
    
       
    
    //get standard deviations from diagonal elements of covariance
    vector < double >
    Xi_a1a3_std, Xi_b1b3_std, Xi_a1b3_std,
    Z_a1a2a3_std, Z_b1b2b3_std, Z_sym_std,
    ZH_a_std, ZH_b_std, ZH_ab_std,
    Q_a1a2a3_std, Q_b1b2b3_std, Q_sym_std;
       
    for(int i=0; i<num_ang_c; i++){
        Xi_a1a3_std.push_back( sqrt(cov_Xi_a1a3[i][i]) );
        Xi_b1b3_std.push_back( sqrt(cov_Xi_b1b3[i][i]) );
        Xi_a1b3_std.push_back( sqrt(cov_Xi_a1b3[i][i]) );

        Z_a1a2a3_std.push_back( sqrt(cov_Z_a1a2a3[i][i]) );
        Z_b1b2b3_std.push_back( sqrt(cov_Z_b1b2b3[i][i]) );
        Z_sym_std.push_back( sqrt(cov_Z_sym[i][i]) );

        ZH_a_std.push_back( sqrt(cov_ZH_a[i][i]) );
        ZH_b_std.push_back( sqrt(cov_ZH_b[i][i]) );
        ZH_ab_std.push_back( sqrt(cov_ZH_ab[i][i]) );

        Q_a1a2a3_std.push_back( sqrt(cov_Q_a1a2a3[i][i]) );
        Q_b1b2b3_std.push_back( sqrt(cov_Q_b1b2b3[i][i]) );
        Q_sym_std.push_back( sqrt(cov_Q_sym[i][i]) );
    }


    
    //=========================================================================================
    if(verbose_c > 0){ cout << endl << "# print 2pc and 3pc" << endl; }
    //=========================================================================================
    
    string filename_out;
    
    //auto: field A
    filename_out = dir_out_c + "3pc_" + suffix_output + "_" + tag_A + ".dat";
    if(verbose_c > 0){ cout << "# " << filename_out << endl; }
    print_correlations(delim_out_c, filename_out, alpha_mean, r1*L_cell_c, r2*L_cell_c, r3_mean, Xi_a1a3, Xi_a1a3_std, Z_a1a2a3, Z_a1a2a3_std, ZH_a, ZH_a_std, Q_a1a2a3, Q_a1a2a3_std);

    
    if(CROSS_CORR){
        
        //auto: field B
        filename_out = dir_out_c + "3pc_" + suffix_output + "_" + tag_B + ".dat";
        if(verbose_c > 0){ cout << "# " << filename_out << endl; }
        print_correlations(delim_out_c, filename_out, alpha_mean, r1*L_cell_c, r2*L_cell_c, r3_mean, Xi_b1b3, Xi_b1b3_std, Z_b1b2b3, Z_b1b2b3_std, ZH_b, ZH_b_std, Q_b1b2b3, Q_b1b2b3_std);
        
        
        //cross: field A X B
        filename_out = dir_out_c + "3pc_" + suffix_output + "_" + tag_A + tag_B + ".dat";
        if(verbose_c > 0){ cout << "# " << filename_out << endl; }
        print_correlations(delim_out_c, filename_out, alpha_mean, r1*L_cell_c, r2*L_cell_c, r3_mean, Xi_a1b3, Xi_a1b3_std, Z_sym, Z_sym_std, ZH_ab, ZH_ab_std, Q_sym, Q_sym_std);
    }
    
    
    //=========================================================================================
    if(verbose_c > 0){ cout << endl << "# print normalized covariances" << endl; }
    //=========================================================================================
    //covariances are normalized in function print_norm_cov()
    
    //auto: field A
    filename_out = dir_out_c + "cov_" + suffix_output + "_" + tag_A + ".dat";    
    if(verbose_c > 0){ cout << "# " << filename_out << endl; }
    print_norm_cov(delim_out_c, filename_out, alpha_mean, r3_mean, cov_Xi_a1a3, cov_Z_a1a2a3, cov_ZH_a, cov_Q_a1a2a3);
    
    if(CROSS_CORR){

        //auto: field B
        filename_out = dir_out_c + "cov_" + suffix_output + "_" + tag_B + ".dat";    
        if(verbose_c > 0){ cout << "# " << filename_out << endl; }
        print_norm_cov(delim_out_c, filename_out, alpha_mean, r3_mean, cov_Xi_b1b3, cov_Z_b1b2b3, cov_ZH_b, cov_Q_b1b2b3);
        
        //cross: field A X B
        filename_out = dir_out_c + "cov_" + suffix_output + "_" + tag_A + tag_B + ".dat";
        if(verbose_c > 0){ cout << "# " << filename_out << endl; }
        print_norm_cov(delim_out_c, filename_out, alpha_mean, r3_mean, cov_Xi_a1b3, cov_Z_sym, cov_ZH_ab, cov_Q_sym);
    }


    return 0;
}
