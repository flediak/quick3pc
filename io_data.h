#include <string>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstring>
#include <vector>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>

using namespace std;


//===========================================================
// check if directory exists
//===========================================================
//from https://stackoverflow.com/questions/4980815/c-determining-if-directory-not-a-file-exists-in-linux
// note: function rquires <string.h> or <cstring>, not <string>
bool directory_exists(const short verbose, const string pathname)
{
    bool dir_ex = false;
    
    struct stat info;
    
    if( stat( pathname.c_str(), &info ) != 0 ){
        if(verbose>2) cout<<"##### WARNING [directory_exists]: cannot access "<< pathname <<" #####"<<endl;
    }else if( info.st_mode & S_IFDIR ){
        dir_ex=true;
    }else{
        if(verbose>2) cout<<"##### WARNING [directory_exists]: "<< pathname << "is no directory #####" <<endl;
    }
    return dir_ex;

}


//===========================================================
// make directory
//===========================================================
//from https://codeyarns.com/2014/08/07/how-to-create-directory-using-c-on-linux/
void make_directory(const short verbose, const string pathname){
    
    const int dir_err = mkdir(pathname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (dir_err == -1)
    {
        printf("#### WARNING [make_directory]: Error creating directory! ####");
        exit(EXIT_FAILURE);
    }else if(verbose > 1){
        cout<<"# make output directory: " << dir_out_c <<endl;
    }
}





//===========================================================
// read binary xyz grid with double precision
//===========================================================
void read_binary_double(const int dim, const int dim_smooth, const string filename, vector < double > &field){

    field.clear();

    if(dim % dim_smooth != 0){
        cout<<"##### ERROR [read_21cmFAST]: dimension of simulation grid and 3pc grid cannot be evenly devided #####"<<endl;
        exit(EXIT_FAILURE);
    }
    
    for(int i=0; i<pow(dim_smooth,3);i++){
        field.push_back(0);
    }
    
    int f_smooth = dim / dim_smooth;
    
    double smooth_norm = pow(f_smooth,3);
    
    //open input file
    FILE *F;
    F = fopen(filename.c_str(), "rb");

    if(F){//check if file is open
        
        //loop over cells
        for (int i=0; i<dim; i++){
            int i_smooth = i / f_smooth;
            for (int j=0; j<dim; j++){
                int j_smooth = j / f_smooth;
                for (int k=0; k<dim; k++){
                    int k_smooth = k / f_smooth;
            
                    double in_val; //input value

                    //read data
                    if(fread(&in_val, sizeof(double), 1, F)){
                        
                        unsigned long ID = pos_to_ID(dim_smooth, i_smooth, j_smooth, k_smooth);

                        if(0 <= ID && ID < field.size()){
                            field[ID] += double(in_val / smooth_norm );
                        }else{
                            cout<<"##### ERROR [read_21cmFAST]: while smoothing input data. Check cell number in input file #####"<<endl;
                            exit (EXIT_FAILURE);
                        }                        
                    }else{
                        cout<<"##### ERROR [read_21cmFAST]: error while reading: " << filename <<" #####"<<endl;
                        exit (EXIT_FAILURE);
                    }                    
                }
            }
        }
        
        fclose(F);

    }else{
        cout<<"##### ERROR [read_21cmFAST]: can't open: " << filename <<" #####"<<endl;
        exit (EXIT_FAILURE);
    }
}



//===========================================================
// read binary xyz grid with float precision
//===========================================================
void read_binary_float(const int dim, const int dim_smooth, const string filename, vector < double > &field){

    field.clear();

    if(dim % dim_smooth != 0){
        cout<<"##### ERROR [read_21cmFAST]: dimension of simulation grid and 3pc grid cannot be evenly devided #####"<<endl;
        exit(EXIT_FAILURE);
    }
    
    for(int i=0; i<pow(dim_smooth,3);i++){
        field.push_back(0);
    }
    
    int f_smooth = dim / dim_smooth;
    
    double smooth_norm = pow(f_smooth,3);
    
    //open input file
    FILE *F;
    F = fopen(filename.c_str(), "rb");

    if(F){//check if file is open
        
        //loop over cells
        for (int i=0; i<dim; i++){
            int i_smooth = i / f_smooth;
            for (int j=0; j<dim; j++){
                int j_smooth = j / f_smooth;
                for (int k=0; k<dim; k++){
                    int k_smooth = k / f_smooth;
            
                    float in_val; //input value

                    //read data
                    if(fread(&in_val, sizeof(float), 1, F)){
                        
                        unsigned long ID = pos_to_ID(dim_smooth, i_smooth, j_smooth, k_smooth);

                        if(0 <= ID && ID < field.size()){
                            field[ID] += double(in_val / smooth_norm );
                        }else{
                            cout<<"##### ERROR [read_21cmFAST]: while smoothing input data. Check cell number in input file #####"<<endl;
                            exit (EXIT_FAILURE);
                        }
                        
                    }else{
                        cout<<"##### ERROR [read_21cmFAST]: error while reading: " << filename <<" #####"<<endl;
                        exit (EXIT_FAILURE);
                    }                    
                }
            }
        }
        
        fclose(F);
            
    }else{
        cout<<"##### ERROR [read_21cmFAST]: can't open: " << filename <<" #####"<<endl;
        exit (EXIT_FAILURE);
    }
}


//===========================================================
// read acii file with pixel data
//===========================================================
void read_ascii_pix(const char delim, string filename, vector < double > &field){

    field.clear();
    
    ifstream indata;
    indata.open(filename.c_str());
    
    if(!indata.is_open()){
        cout<<"##### ERROR [read_ascii_pix]: can't open: " << filename <<" #####"<<endl;
        exit (EXIT_FAILURE);
    }else{
        
        string var_str;
        
        //loop over variables in input file
        while(getline(indata,var_str,delim)){
            double var = stod(var_str);

            if(isnan(var)){
                cout<<"##### ERROR [read_ascii_pix]: NaN in " << filename <<" #####"<<endl;
                exit (EXIT_FAILURE);
            }
            if(isinf(var)){
                cout<<"##### ERROR [read_ascii_pix]: inf in " << filename <<" #####"<<endl;
                exit (EXIT_FAILURE);
            }

            field.push_back(var);
        }

        indata.close();
    }        
}


//===========================================================
// read acii file with xyz data
//===========================================================
void read_ascii_xyz(
    const short verbose, const double l_box, const int dim_cell,
    const char delim, string filename, vector < double > &field){

    double l_cell = l_box / double(dim_cell);
    
    //clear field and initialize to zero
    field.clear();
    field.assign(pow(dim_cell,3),0);
    
    ifstream indata;
    indata.open(filename.c_str());
    
    int num_points;
    
    if(!indata.is_open()){
        cout<<"##### ERROR [read_ascii_xyz]: can't open: " << filename <<" #####"<<endl;
        exit (EXIT_FAILURE);
    }else{
       
        //------- loop over lines in input file -------
        int lineNr = 0;

        string line;
        while (getline(indata, line)){
            
            //loop over variables in line
            stringstream  ss(line);
            string var;
            int columnNr = 0;
            double x,y,z;

            while(getline(ss,var,delim)){

                //check input
                if(isnan(stod(var))){
                    cout<< "#### ERROR [read_ascii_xyz]: NaN in line " << lineNr << ", column " << columnNr <<" ####" <<endl;
                    exit(EXIT_FAILURE);
                }
                if(isinf(stod(var))){
                    cout<< "#### ERROR [read_ascii_xyz]: inf line " << lineNr << ", column " << columnNr << " ####" << endl;
                    exit(EXIT_FAILURE);
                }

                if( 0 > stod(var) || stod(var) > l_box){
                    cout<< "#### ERROR [read_ascii_xyz]: points outside of box ####" <<endl;
                    exit(EXIT_FAILURE);                    
                }
                
                //convert string to data types
                if(columnNr == 0){ x = stod(var); }
                if(columnNr == 1){ y = stod(var); }
                if(columnNr == 2){ z = stod(var); }
        
                columnNr++;
            }
            
            lineNr++;
            
            int i = int( x / l_cell); 
            int j = int( y / l_cell); 
            int k = int( z / l_cell);
            
            unsigned long ID_cell = pos_to_ID(dim_cell, i, j, k);

            field[ID_cell] = field[ID_cell] + 1;
        }
        //---------------------------------------------
        
        num_points = lineNr;
    }
    
    if(verbose > 0){ cout<<"# - "<< num_points <<" data points in "<< filename <<endl; }
}




//===========================================================
// print correlation function into acii file
//===========================================================
void print_correlations(
    const char delim,
    const string filename,
    const vector < double > alpha_mean,
    const double r1,
    const double r2,
    const vector < double > r3_mean,
    const vector < double > Xi,
    const vector < double > Xi_std,
    const vector < double > Z,
    const vector < double > Z_std,
    const vector < double > ZH,
    const vector < double > ZH_std,
    const vector < double > Q,
    const vector < double > Q_std)
{
    ofstream outdata;
    outdata.open(filename.c_str());
    outdata.setf(ios::fixed);
    outdata.precision (8);

    int num_ang = alpha_mean.size();
    double dalpha = 180./double(num_ang);

    for(int i = 0; i < Xi.size(); i++){

        double alpha_i = (i+0.5)*dalpha;

        outdata
        << i << delim                                           //1
        << alpha_i << delim << alpha_mean[i] << delim           //2,3
        << r1 << delim << r2 << delim << r3_mean[i] << delim    //4,5,6
        << Xi[i] << delim << Xi_std[i] << delim                 //7,8
        << Z[i] << delim << Z_std[i] << delim                   //9,10
        << ZH[i] << delim << ZH_std[i] << delim                 //11,12
        << Q[i] << delim << Q_std[i];                           //13,14
        if(i < Xi.size()-1){ outdata<<endl; }//avoid new line at the end of the file

    }    
}



//===========================================================
// print normalized covariances into acii file
//===========================================================
void print_norm_cov(
    const char delim,
    const string filename,
    const vector < double > alpha_mean,
    const vector < double > r3_mean,
    const vector < vector < double > > cov_Xi,
    const vector < vector < double > > cov_Z,
    const vector < vector < double > > cov_ZH,
    const vector < vector < double > > cov_Q)
{
    ofstream outdata;
    outdata.open(filename.c_str());
    outdata.setf(ios::fixed);
    outdata.precision (8);
        
    int num_ang = alpha_mean.size();
    double dalpha = 180./double(num_ang);


    for(int i = 0; i < cov_Xi.size(); i++){
        for(int j = 0; j < cov_Xi[i].size(); j++){

            double alpha_i = (i+0.5)*dalpha;
            double alpha_j = (j+0.5)*dalpha;
            
            outdata
            << i << delim << j << delim                                             //1,2
            << alpha_i << delim << alpha_j << delim                                 //3,4
            << alpha_mean[i] << delim << alpha_mean[j] << delim                     //5,6
            << r3_mean[i] << delim << r3_mean[j] << delim                           //7,8
            << cov_Xi[i][j] / sqrt(cov_Xi[i][i]) / sqrt(cov_Xi[j][j]) << delim      //9
            << cov_Z[i][j] / sqrt(cov_Z[i][i]) / sqrt(cov_Z[j][j]) << delim         //10
            << cov_ZH[i][j] / sqrt(cov_ZH[i][i]) / sqrt(cov_ZH[j][j])<< delim       //11
            << cov_Q[i][j] / sqrt(cov_Q[i][i]) / sqrt(cov_Q[j][j]);                 //12
            if(i < cov_Xi.size()-1 || j < cov_Xi[i].size()-1){ outdata<<endl; }//avoid new line at the end of the file
        }
    }

}



