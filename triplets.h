#include <stdlib.h> 
#include <vector>

using namespace std;


//==============================================================
// compute 3-point cross-correlation
//==============================================================

void compute_3pc(
    const short verbose,
    bool periodic_box,
    const int dim_cell,
    const int dim_JKcell,
    const vector < double > dc_a,
    const vector < vector < int > > dvec1,
    const vector < vector < int > > dvec2,
    const vector < vector < short > > alpha_bin,
    vector < double > &Xi_a1a2,
    vector < double > &Xi_a2a3,
    vector < vector < double > > &Xi_a1a3,
    vector < vector < double > > &Z_a1a2a3){  
    
    double Xi_a2a3_bin=0, Xi_a1a3_bin=0, Z_a1a2a3_bin=0;

    if(verbose > 1){cout<<"# remaining cells on x-axis: " << endl;}

    //loop over cells in simulation box = fist triangle cell
    for(int i1 = 0; i1 < dim_cell; i1++){

        if(verbose > 1){ print_cell_num(dim_cell, i1); }

        for(int j1 = 0; j1 < dim_cell; j1++){
            for(int k1 = 0; k1 < dim_cell; k1++){

                //get ID of JK sample in which first triangle cell falls
                int iJK = int(i1/(dim_cell/dim_JKcell));
                int jJK = int(j1/(dim_cell/dim_JKcell));
                int kJK = int(k1/(dim_cell/dim_JKcell));
                int JKsamp = pos_to_ID(dim_JKcell, iJK, jJK, kJK);
                
                unsigned long ID_cell_1 = pos_to_ID(dim_cell, i1, j1, k1);
                double a1 = dc_a[ID_cell_1];
                               
                //loop over displacement vectors of shell 1
                for(int s1 = 0; s1 < dvec1.size(); s1 ++){
                    
                    //add first displacement vector
                    int i2 = i1 + dvec1[s1][0];
                    int j2 = j1 + dvec1[s1][1];
                    int k2 = k1 + dvec1[s1][2];
                    
                    if(periodic_box){
                        make_periodic(dim_cell, i2);
                        make_periodic(dim_cell, j2);
                        make_periodic(dim_cell, k2);
                    }
                    
                    //check if second cell is inside simulation box
                    if(in_box(0, dim_cell, i2, j2, k2)){
                            
                        unsigned long ID_cell_2 = pos_to_ID(dim_cell, i2, j2, k2);
                        double a2 = dc_a[ID_cell_2];
                        
                        Xi_a1a2[JKsamp] = Xi_a1a2[JKsamp] + a1*a2;

                        //loop over displacement vectors of shell 2
                        for(int s2 = 0; s2 < dvec2.size(); s2 ++){
                            
                            //add second displacement vector
                            int i3 = i2 + dvec2[s2][0];
                            int j3 = j2 + dvec2[s2][1];
                            int k3 = k2 + dvec2[s2][2];

                            if(periodic_box){
                                make_periodic(dim_cell, i3);
                                make_periodic(dim_cell, j3);
                                make_periodic(dim_cell, k3);
                            }
                            
                            //check if third cell is inside simulation box
                            if(in_box(0, dim_cell, i3, j3, k3)){
                                
                                unsigned long ID_cell_3 = pos_to_ID(dim_cell, i3, j3, k3);
                                double a3 = dc_a[ID_cell_3];
                                short ang_bin = alpha_bin[s1][s2];

                                Xi_a2a3[JKsamp] = Xi_a2a3[JKsamp] + a2*a3;
                                Xi_a1a3[JKsamp][ang_bin] = Xi_a1a3[JKsamp][ang_bin] + a1*a3;
                                Z_a1a2a3[JKsamp][ang_bin] = Z_a1a2a3[JKsamp][ang_bin] + a1*a2*a3;
   
                            }
                        }
                    }
                }
            }
        }
    }
   
    cout<<endl;
}

//==============================================================
// compute 3-point cross-correlation
//==============================================================

void compute_3pcc(
    const short verbose,
    bool periodic_box,
    const int dim_cell,
    const int dim_JKcell,
    const vector < double > dc_a,
    const vector < double > dc_b,
    const vector < vector < int > > dvec1,
    const vector < vector < int > > dvec2,
    const vector < vector < short > > alpha_bin,
    vector < double > &Xi_a1b2,
    vector < double > &Xi_a2b3,
    vector < vector < double > > &Xi_a1b3,
    vector < vector < double > > &Z_a1b2b3,
    vector < vector < double > > &Z_b1a2b3,
    vector < vector < double > > &Z_b1b2a3
){  

    if(verbose > 1){cout<<"# remaining cells on x-axis: " << endl;}
    
    //loop over cells in simulation box = fist triangle cell
    for(int i1 = 0; i1 < dim_cell; i1++){

        if(verbose > 1){ print_cell_num(dim_cell, i1); }

        for(int j1 = 0; j1 < dim_cell; j1++){
            for(int k1 = 0; k1 < dim_cell; k1++){

                //get ID of JK sample in which first triangle cell falls
                int iJK = int(i1/(dim_cell/dim_JKcell));
                int jJK = int(j1/(dim_cell/dim_JKcell));
                int kJK = int(k1/(dim_cell/dim_JKcell));
                int JKsamp = pos_to_ID(dim_JKcell, iJK, jJK, kJK);
                
                unsigned long ID_cell_1 = pos_to_ID(dim_cell, i1, j1, k1);
                double a1 = dc_a[ID_cell_1];
                double b1 = dc_b[ID_cell_1];
                
                //loop over displacement vectors of shell 1
                for(int s1 = 0; s1 < dvec1.size(); s1 ++){
                    
                    //add first displacement vector
                    int i2 = i1 + dvec1[s1][0];
                    int j2 = j1 + dvec1[s1][1];
                    int k2 = k1 + dvec1[s1][2];
                    
                    if(periodic_box){
                        make_periodic(dim_cell, i2);
                        make_periodic(dim_cell, j2);
                        make_periodic(dim_cell, k2);
                    }
                    
                    //check if second cell is inside simulation box
                    if(in_box(0, dim_cell, i2, j2, k2)){
                            
                        unsigned long ID_cell_2 = pos_to_ID(dim_cell, i2, j2, k2);
                        double a2 = dc_a[ID_cell_2];
                        double b2 = dc_b[ID_cell_2];
                            
                        Xi_a1b2[JKsamp] = Xi_a1b2[JKsamp] + (a1*b2 + b1*a2)/2.;
                        
                        //loop over displacement vectors of shell 2
                        for(int s2 = 0; s2 < dvec2.size(); s2 ++){
                            
                            //add second displacement vector
                            int i3 = i2 + dvec2[s2][0];
                            int j3 = j2 + dvec2[s2][1];
                            int k3 = k2 + dvec2[s2][2];

                            if(periodic_box){
                                make_periodic(dim_cell, i3);
                                make_periodic(dim_cell, j3);
                                make_periodic(dim_cell, k3);
                            }
                            
                            //check if third cell is inside simulation box
                            if(in_box(0, dim_cell, i3, j3, k3)){

                                unsigned long ID_cell_3 = pos_to_ID(dim_cell, i3, j3, k3);
                                double a3 = dc_a[ID_cell_3];
                                double b3 = dc_b[ID_cell_3];
                                
                                short ang_bin = alpha_bin[s1][s2];

                                Xi_a2b3[JKsamp] = Xi_a2b3[JKsamp] + (a2*b3 + b2*a3)/2.;

                                Xi_a1b3[JKsamp][ang_bin] = Xi_a1b3[JKsamp][ang_bin] + (a1*b3 + b1*a3)/2.;

                                Z_a1b2b3[JKsamp][ang_bin] = Z_a1b2b3[JKsamp][ang_bin] + a1*b2*b3;
                                Z_b1a2b3[JKsamp][ang_bin] = Z_b1a2b3[JKsamp][ang_bin] + b1*a2*b3;
                                Z_b1b2a3[JKsamp][ang_bin] = Z_b1b2a3[JKsamp][ang_bin] + b1*b2*a3;
                                
                            }
                        }
                    }
                }
            }
        }
    }
    cout<<endl;
}


//==============================================================
// compute counts of pairs and triangles
//==============================================================

void get_3pc_norm(
    const short verbose,
    bool periodic_box,
    const int dim_cell,
    const int dim_JKcell,
    const vector < vector < int > > dvec1,
    const vector < vector < int > > dvec2,
    const vector < vector < short > > alpha_bin,
    vector < double > &norm_12,
    vector < double > &norm_23,
    vector < vector < double > > &norm_13,
    vector < vector < double > > &norm_123
){

    if(verbose > 1){cout<<"# remaining cells on x-axis: " << endl;}
    
    //loop over cells in simulation box = fist triangle cell
    for(int i1 = 0; i1 < dim_cell; i1++){

        if(verbose > 1){ print_cell_num(dim_cell, i1); }

        for(int j1 = 0; j1 < dim_cell; j1++){
            for(int k1 = 0; k1 < dim_cell; k1++){

                //get ID of JK sample in which first triangle cell falls
                int iJK = int(i1/(dim_cell/dim_JKcell));
                int jJK = int(j1/(dim_cell/dim_JKcell));
                int kJK = int(k1/(dim_cell/dim_JKcell));
                int JKsamp = pos_to_ID(dim_JKcell, iJK, jJK, kJK);

                //loop over displacement vectors of shell 1
                for(int s1 = 0; s1 < dvec1.size(); s1 ++){
                    
                    //add first displacement vector
                    int i2 = i1 + dvec1[s1][0];
                    int j2 = j1 + dvec1[s1][1];
                    int k2 = k1 + dvec1[s1][2];

                    if(periodic_box){
                        make_periodic(dim_cell, i2);
                        make_periodic(dim_cell, j2);
                        make_periodic(dim_cell, k2);
                    }

                    //check if second cell is inside simulation box
                    if(in_box(0, dim_cell, i2, j2, k2)){

                        norm_12[JKsamp] = norm_12[JKsamp] + 1;

                        //loop over displacement vectors of shell 1
                        for(int s2 = 0; s2 < dvec2.size(); s2 ++){

                            //add second displacement vector
                            int i3 = i2 + dvec2[s2][0];
                            int j3 = j2 + dvec2[s2][1];
                            int k3 = k2 + dvec2[s2][2];

                            if(periodic_box){
                                make_periodic(dim_cell, i3);
                                make_periodic(dim_cell, j3);
                                make_periodic(dim_cell, k3);
                            }
                            
                            //check if third cell is inside simulation box
                            if(in_box(0, dim_cell, i3, j3, k3)){
                                
                                short ang_bin = alpha_bin[s1][s2];
                                
                                norm_23[JKsamp] = norm_23[JKsamp] + 1;
                                norm_123[JKsamp][ang_bin] = norm_123[JKsamp][ang_bin] + 1;
                            }
                        }
                    }
                }
            }
        }
    }
    
    norm_13 = norm_123;
    
    cout<<endl;
}

//------------------------------------------------------------------
//------------------------------------------------------------------



