#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

//===========================================================
// sum up results from all jk cells (for 2pc at fixed skales)
//===========================================================
void sumJKcells_1Dvec(const vector < double > vec, double &sum){
    
    sum = 0;
    
    for(int i=0; i < vec.size(); i++){
        sum = sum + vec[i];
    }
}


//===========================================================
// sum up results from all jk cells (for 2pc and 3pc with angular dependence)
//===========================================================
void sumJKcells_2Dvec(const vector < vector < double > > vec, vector < double > &sum){
    
    //initialize sum
    sum.clear();
    sum.assign(vec[0].size(),0);
    
    
    for(int i=0; i < vec.size(); i++){
        for(int j=0; j < vec[i].size(); j++){
            sum[j] = sum[j] + vec[i][j];
        }
    }
}


//===========================================================
// sum up results from all jk cells, excluding one JK cell  (for 2pc at fixed skales)
//===========================================================
void makeJKsamp_1Dvec(const vector < double > vec, vector < double > &sum){
    
    if(vec.size() != sum.size()){
        cout<<"#### ERROR: input vectors in makeJKsamp_1Dvec do not have the same dimension"<<endl;
        exit(0);
    }

    for(int i=0; i < sum.size(); i++){
        sum[i] = 0;
    }

        
    for(int i=0; i < vec.size(); i++){
        for(int j=0; j < vec.size(); j++){
            if(i != j){
                sum[i] = sum[i] + vec[j];
            }
        }
    }
}


//===========================================================
//  sum up results from all jk cells, excluding one JK cell (for 2pc and 3pc with angular dependence)
//===========================================================
void makeJKsamp_2Dvec(const vector < vector < double > > vec, vector < vector < double > > &sum){
    
    if(vec.size() != sum.size()){
        cout<<"#### ERROR: input vectors in makeJKsamp_2Dvec do not have the same dimension"<<endl;
        exit(0);
    }

    for(int i=0; i < sum.size(); i++){
        for(int j=0; j < sum[i].size(); j++){
            sum[i][j] = 0;
        }
    }

    for(int i=0; i < vec.size(); i++){
        for(int j=0; j < vec.size(); j++){
            if(i != j){
                for(int k=0; k < vec[j].size(); k++){
                    sum[i][k] = sum[i][k] + vec[j][k];
                }
            }
        }
    }
    
}




//===========================================================
// make symmetric 3pc cross-correlation for JK samples
//===========================================================
void make_Z_sym_JK(
    const vector < vector < double > > Z_a1b2b3_JK,
    const vector < vector < double > > Z_b1a2b3_JK, 
    const vector < vector < double > > Z_b1b2a3_JK,
    vector < vector < double > > &Z_sym_JK)
{
    Z_sym_JK.clear();
    
    //loop over JK samples
    for(int i=0; i<Z_a1b2b3_JK.size(); i++){

        vector < double > Z_sym_JK_sub;

        //loop over angular bins
        for(int j=0; j<Z_a1b2b3_JK[i].size(); j++){
        
            double Z_sym = (Z_a1b2b3_JK[i][j] + Z_b1a2b3_JK[i][j] + Z_b1b2a3_JK[i][j]) / 3.; 
                
            Z_sym_JK_sub.push_back(Z_sym);
        }
        Z_sym_JK.push_back(Z_sym_JK_sub);
    }
}




//===========================================================
// make hierarchical 3pc for JK samples
//===========================================================
void make_ZH_JK(
    const vector < double > Xi_12_JK,
    const vector < double > Xi_23_JK,
    const vector < vector < double > > Xi_13_JK,
    vector < vector < double > > &ZH_JK
){

    ZH_JK.clear();
    
    //loop over JK samples
    for(int i=0; i<Xi_13_JK.size(); i++){

        vector < double > ZH_JK_sub;

        //loop over angular bins
        for(int j=0; j<Xi_13_JK[i].size(); j++){
            
            double ZH = 
            Xi_12_JK[i]*Xi_13_JK[i][j] +
            Xi_12_JK[i]*Xi_23_JK[i] +
            Xi_23_JK[i]*Xi_13_JK[i][j];
            
            ZH_JK_sub.push_back(ZH);
        }
        ZH_JK.push_back(ZH_JK_sub);
    }
}



//===========================================================
// make reduced 3pc for JK samples
//===========================================================
void make_Q_JK(
    const vector < vector < double > > Z_JK,
    const vector < vector < double > > ZH_JK,
    vector < vector < double > > &Q_JK
){

    Q_JK.clear();
    
    //loop over JK samples
    for(int i=0; i<Z_JK.size(); i++){

        vector < double > Q_JK_sub;

        //loop over angular bins
        for(int j=0; j<Z_JK[i].size(); j++){
            
            double Q = 0;
            
            if(ZH_JK[i][j] != 0){
               Q =  Z_JK[i][j] / ZH_JK[i][j];
            }
            
            Q_JK_sub.push_back(Q);
        }
        Q_JK.push_back(Q_JK_sub);
    }
}


//===========================================================
// make JK covariance
//===========================================================

void make_JKcov(
    const vector < double > vec,
    const vector < vector < double > > vec_JKsamp,
    vector < vector < double > > &cov){
    
    int num_bin = vec.size();
    int num_JKsamp = vec_JKsamp.size();
    
    init_vec2D(cov, num_bin, num_bin, 0);

    for(int i=0; i < num_bin; i++){
        for(int j=0; j < num_bin; j++){
            for(int k=0; k < num_JKsamp; k++){
                cov[i][j] = cov[i][j] +
                (vec_JKsamp[k][i]-vec[i]) * (vec_JKsamp[k][j]-vec[j]) * double (num_JKsamp - 1) / double (num_JKsamp);
            }
        }
    }
    
}
