

#comile quick3PC
#make quick3PC

#comile all (quick3PC make_rand_cat)
make


#generate catalogue with random xyz data
./make_rand_cat ./data/rand_xyz_A.dat
./make_rand_cat ./data/rand_xyz_B.dat

#compute 2- and 3-point auto correlation
#./quick3PC 6 0.25 3 0.25 random alpha ./data/rand_xyz_A.dat

#compute 2- and 3-point auto and cross-correlations
./quick3PC 6 0.25 3 0.25 random m T ./data/rand_xyz_A.dat ./data/rand_xyz_B.dat
