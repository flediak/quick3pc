# C++ compiler and flags
LDFLAGS = -std=c++11 -O3
CC      = g++

PARAMETER_FILE = ./parameters.h

all: quick3PC make_rand_cat


#------------------------------------------------------
quick3PC: quick3PC.cpp ${PARAMETER_FILE} quick3PC.h io_data.h triplets.h jack_knife.h
	${CC} quick3PC.cpp -o quick3PC ${LDFLAGS}

make_rand_cat: make_rand_cat.cpp ${PARAMETER_FILE} io_data.h
	${CC} make_rand_cat.cpp -o make_rand_cat ${LDFLAGS}


distclean: clean all
