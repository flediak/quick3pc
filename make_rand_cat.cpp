#include <iostream>
#include <math.h>
#include <string>
#include <iostream>
#include <fstream>
#include "./parameters.h"

using namespace std;


//===========================================================
// make ascii file with random values between 0 and 1 in pixels
//===========================================================
//can be read with function read_ascii_pix() in io_data.h

void make_random_ascii_pix(const int DIM, const char delim, string output_file){

    int digits = 6;
    int prcn = pow(10,digits);
    
    //seed random number generator with current time
    srand(time(NULL));
    
    //open file
    ofstream outdata;
    outdata.open(output_file.c_str());
    
    if(outdata.is_open()){

        outdata.setf(ios::fixed);
        outdata.precision (digits);

        //loop over cells
        for (int i=0; i<DIM; i++){
            for (int j=0; j<DIM; j++){
                for (int k=0; k<DIM; k++){
                    
                    //generate random number between 0 and 1
                    double random = (rand() % prcn)/double(prcn);
                    
                    //write nmber in output file
                    outdata << random << delim;

                }
            }
        }
        outdata.close();
    
    }else{
        cout<<"#### ERROR [make_random_ascii_pix]: can't write " << output_file <<"####"<<endl;
        exit (EXIT_FAILURE);
    }

}


//===========================================================
// make ascii file with random xyz points
//===========================================================
//can be read with function read_ascii_xyz() in io_data.h

void make_random_ascii_xyz(const double l_box, const long num_point, const char delim, string output_file){

    int digits = 6;
    int prcn = pow(10,digits);
    
    //seed random number generator with current time
    srand(time(NULL));

    //open file
    ofstream outdata;
    outdata.open(output_file.c_str());
    
    if(outdata.is_open()){

        outdata.setf(ios::fixed);
        outdata.precision (digits);

        //loop over cells
        for (int i=0; i<num_point; i++){
                    
            //generate random number between 0 and Lbox
            double x = (rand() % prcn)/double(prcn)*l_box;
            double y = (rand() % prcn)/double(prcn)*l_box;
            double z = (rand() % prcn)/double(prcn)*l_box;
            
            //write nmber in output file
            outdata << x << delim  << y << delim  << z;
            if(i<num_point-1){outdata<<endl;}
        }
        
        outdata.close();
        
    }else{
        cout<<"#### ERROR [make_random_ascii_xyz]: can't write " << output_file <<"####"<<endl;
        exit (EXIT_FAILURE);
    }
        
}



//===================================================================================================
//=========================================   MAIN CODE   ===========================================
//===================================================================================================
int main(int argc, char **argv){

    cout << endl;
    cout << "#===========================================================" << endl;
    cout << "#===================== make random 3PC =====================" << endl;
    cout << "#===========================================================" << endl << endl;

    //check input
    if(argc != 2){
        cout<<"# USAGE : ./make_rand_cat <filename_out>" << endl;
        return -1;
    }

    //read input filename
    string filename_out = argv[1];

    //const string output_format_c = "ASCII_PIX";
    //const string output_format_c = "ASCII_XYZ";
    const string output_format_c = input_format_A_c;
    

    int num_point = 1e8;//number of random points for xyz output

    
    cout << endl <<"# make random field: " << filename_out << endl;
    cout <<"# output format: " << output_format_c << endl;

    
    //make random catalogue with random points
    if(output_format_c == "ASCII_XYZ"){
        cout <<"# number of random points: " << num_point << endl;
        make_random_ascii_xyz(L_box_c, num_point, delim_in_c, filename_out);
    }
    
    //make random catalogue with random values in pixels
    if(output_format_c == "ASCII_PIX"){
        make_random_ascii_pix(dim_cell_c, delim_in_c, filename_out);
    }
    
    if(output_format_c != "ASCII_XYZ" && output_format_c != "ASCII_PIX"){
        cout << "#### ERROR: unknown output format: " << output_format_c << " ####" << endl;
    }
    
    return 0;
}
